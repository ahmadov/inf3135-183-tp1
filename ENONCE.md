# Travail pratique 1 : `connect4`

Il s'agit de l'énoncé du travail pratique 1 du cours INF3135 Construction et
maintenance de logiciels, enseigné par Alexandre Terrasa à l'automne 2018,
pour l'Université du Québec à Montréal.

Le travail est à effectuer **seul** et doit être remis au plus tard le:

* **5 octobre à 23h59** pour le groupe 21
* **7 octobre à 23h59** pour le groupe 40

À partir de minuit, une pénalité de **2%** par heure de retard sera appliquée.

## Objectif

Vous devez implémenter le jeu [Connect 4](https://fr.wikipedia.org/wiki/Puissance_4) en C.
Le jeu se jouera dans la console grâce à `stdin` et `stdout`.

## Présentation de la grille

Le jeu se joue dans une grille comptant 6 rangées de 7 colonnes.
La grille est affichée dans la sortie standard (`stdout`) selon la représentation suivante:

~~~

  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

~~~

## Tours de jeu

Tour à tour les deux joueurs placent un disque dans la colonne de leur choix,
le disque tombe alors jusqu'à la position la plus base possible dans la dite colonne
à la suite de quoi c'est à l'adversaire de jouer.

Dans le terminal, les couleurs des joueurs sont représentées par les caractères suivants:

* Joueur 1: `X`
* Joueur 2: `O`

Le programme affiche la grille puis invite le joueur `X` à saisir la colonne dans
laquelle il veut placer son disque:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

Player X action:
~~~

Le joueur `X` saisit alors sa colonne grâce à l'entrée standard (`stdin`).
Par exemple `3`.

Puis c'est au joueur `O` de jouer.
Le programme affiche la grille mise à jour et invite le joueur `O` à saisir sa colonne:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|     X         |
-----------------

Player O action:
~~~

Le joueur `O` saisit alors sa colonne grâce à l'entrée standard (`stdin`).
Par exemple `4`.

Puis c'est au joueur `X` de jouer.

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|     X O       |
-----------------

Player X action: 4


  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|       X       |
|     X O       |
-----------------

Player O action:
~~~

Et ainsi de suite...

## Fin du jeu

Le jeu termine dés que l'une des conditions suivantes est rencontrée:

* Un des deux joueurs saisit la colonne `0`
* Un des deux joueurs parvient à aligner 4 disques
* La grille est pleine

### Quitter le jeu

Afin de permettre aux joueurs de quitter la partie, une colonne spéciale `0` peut
être saisie lors de la phase d'action.

Ainsi, si le joueur tape 0, le programme affiche `exit` et termine:

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
|               |
|               |
-----------------

Player X action: 0
exit
~~~

### Victoire

Pour gagner, le joueur doit aligner 4 disques de sa couleur dans la grille.
Les disques peuvent être alignés horizontalement, verticalement ou en diagonale.

Dans l'exemple suivant, le joueur `X` gagne grâce à 4 disques alignés horizontalement.
Le programme affiche alors `Player X wins!` et termine.

~~~
  1 2 3 4 5 6 7
|               |
|               |
|               |
|               |
| O O O         |
| X X X X       |
-----------------

Player X wins!
~~~

Ici, le joueur `X` gagne grâce à 4 disques alignés verticalement:

~~~
  1 2 3 4 5 6 7
|               |
|               |
| X             |
| X O           |
| X O           |
| X O           |
-----------------

Player X wins!
~~~

Il faut aussi prendre en compte les diagonales:

~~~
  1 2 3 4 5 6 7
|               |
|               |
| O             |
| X O           |
| O O O         |
| X X X O X X   |
-----------------

Player O wins!
~~~

### Grille pleine

Si la grille est pleine, plus aucune action n'est possible.
Le programme affiche alors la grille pleine suivie de `It's a draw...` puis termine.

~~~
  1 2 3 4 5 6 7
| O O O X O O O |
| X X X O X X X |
| O O O X O O O |
| X X X O X X X |
| O O O X O O O |
| X X X O X X X |
-----------------

It's a draw...
~~~

## Gestion des erreurs

Votre programme devra gérer certains cas d'erreurs en affichant **exactement**
les messages d'erreurs demandés (sinon les tests automatisés vont échouer):

1. Si l'utilisateur fournit des arguments au programme, alors le message
   d'erreur devrait être:
   ```sh
   Error: No argument expected.
   ```
   Puis le programme doit s'arrêter.

2. Si l'utilisateur fournit un numéro de colonne invalide, alors le message d'erreur
   devrait être:
   ```sh
   Error: action should be an integer between `1` and `7`.
   ```
   Puis le programme demande à nouveau à l'utilisateur de saisir sa colonne.

3. Si l'utilisateur sélectionne une colonne déjà pleine, alors on doit plutôt écrire:
   ```sh
   Error: the column `X` is full.
   ```
   où `X` est à remplacer par le numéro de la colonne.

   Puis le programme demande à nouveau à l'utilisateur de saisir sa colonne.

## Marche à suivre

Afin de compléter ce travail pratique, vous devrez suivre les étapes suivantes
(pas nécessairement dans l'ordre):

1. Clonez (mais sans faire de *fork*) le [dépôt du
   projet](https://gitlab.com/Morriar/inf3135-183-tp1).
2. Créez un dépôt nommé `inf3135-183-tp1`.
3. Implémentez le programme `connect4.c` en suivant le standard C11.
4. Complétez le `Makefile` de l'application pour qu'il compile votre programme
   `connect4.c`.
5. Complétez le fichier `README.md` en respectant le format Markdown.
6. Versionnez fréquemment l'évolution de votre projet avec Git.

### Clone et création du dépôt

Vous devez cloner le dépôt fourni et l'héberger sur la plateforme
[GitLab](https://gitlab.com/). Ne faites pas de *fork*, car cela pourra
entraîner certains problèmes (nous verrons les *forks* dans le TP2). Votre
dépôt devra se nommer **exactement** `inf3135-183-tp1` et l'URL devra être
**exactement** `https://gitlab.com/<utilisateur>/inf3135-183-tp1`, où
`<utilisateur>` doit être remplacé par votre identifiant GitLab. Il devra être
**privé** et accessible seulement par vous et par `Morriar`
(mon nom d'utilisateur GitLab).

### Programme `connect4.c`

Votre programme doit être écrit dans un seul fichier nommé `connect4.c`, placé
dans le répertoire `src`.  Le programme doit être écrit en C, compatible avec
C11 et compilable avec `gcc` version 4.8 ou ultérieure. Assurez-vous qu'il
fonctionne correctement sur les serveurs Malt ou Java.

### Makefile

Vous devez ajouter une règle dans le fichier Makefile pour compiler votre
programme. N'oubliez pas les dépendances s'il y en a!

La règle devrait avoir comme cible `bin/connect4` (l'exécutable), qui doit être
produit en compilant le fichier `src/connect4.c`.

### Fichier `README.md`

Vous devez modifier le fichier `README.md` pour ajouter différentes
informations. Assurez-vous de répondre minalement aux questions suivantes, en
plus de celles qui y sont mentionnées:

* **Votre code permanent UQAM**
* À quoi sert votre programme?
* Comment le compiler?
* Comment l'exécuter?
* Quels sont les formats d'entrées et de sorties?
* Quels sont les cas d'erreurs gérés?

Vous devez utiliser le format Markdown pour écrire une documentation claire et
lisible. Soignez la qualité de votre français, qui sera évalué plus
particulièrement dans le fichier `README.md`.

### Git

Les sources de votre programme devront être versionnées à l'aide de Git.  Vous
devez cloner le gabarit du projet fourni et rajouter vos modifications à l'aide
de *commits*.  N'oubliez pas de modifier le fichier `.gitignore` en fonction de
votre environnement de développement. Aussi, assurez-vous de ne pas versionner
de fichiers inutiles!

Adresse du dépôt à clôner:
[https://gitlab.com/Morriar/inf3135-183-tp1](https://gitlab.com/Morriar/inf3135-183-tp1).

## Correction

L'exécution de votre programme sera vérifiée automatiquement grâce au script
`tests.sh` fourni avec le gabarit du projet. Notez que vous n'avez pas à
connaître la syntaxe d'un script shell pour pouvoir l'utiliser.

Pour faciliter votre développement, vous avez accès à dix des tests utilisés
pour corriger vos travaux (répertoire `tests/`) ainsi qu'au script de tests
(`tests.sh`). Il suffit d'entrer `make check` pour lancer la suite de tests.

### Barème

Les critères d'évaluation sont les suivants:

| Critère            | Points |
| -------            | -----: |
| Fonctionnabilité   | /40    |
| Qualité du code    | /20    |
| Documentation      | /15    |
| Makefile           | /10    |
| Utilisation de Git | /15    |
| Total              | /100   |

Les critères plus détaillés sont décrits ci-bas:

* **Fonctionnabilité (40%)**: 20 tests, 2 points par test. Le programme passe
  les tests en affichant le résultat attendu.

* **Qualité du code (20%)**: Les identifiants utilisés sont significatifs et
  uniformes, le code est bien indenté, il y a de l'aération autour des
  opérateurs et des parenthèses, le programme est simple et lisible. Pas de
  code commenté ou de commentaires inutiles. Il est décomposé en petites
  fonctions qui effectuent des tâches spécifiques. La présentation est soignée
  de façon générale.

* **Documentation (15%)**: Le fichier `README.md` est complet et respecte le
  format Markdown. L'en-tête du fichier `connect4.c` est bien documentée, de
  même que chacune des fonctions (*docstrings*) en suivant le standard Javadoc.

* **Makefile (10%)**: Le Makefile permet de compiler le programme avec la
  commande `make bin/connect4` et de lancer les tests automatiquement avec
  `make check`.

* **Utilisation de Git (15%)**: Les modifications sont réparties en plusieurs
  *commits*. Le fichier `.gitignore` a été mis à jour. Les messages de *commit*
  sont significatifs et respectent le format suggéré (première ligne courte,
  suivi de paragraphes si nécessaires).

### Pénalités

Tout programme ne passant pas au moins le test 0 (compilation via Makefile) se
verra automatiquement attribuer **la note 0**.  Vous devez vous assurer que
votre programme passe au moins ce test grâce au script `tests.sh` fourni.

En outre, si vous ne respectez pas les critères suivants, une pénalité de
**50%** sera imposée :

- Votre dépôt doit se nommer **exactement** `inf3135-183-tp1` sur GitLab;

- L'URL de votre dépôt doit être **exactement**
  `https://gitlab.com/<utilisateur>/inf3135-183-tp1` où `<utilisateur>`
  doit être remplacé par votre identifiant GitLab.

- Votre dépôt doit être **privé**.

- L'utilisateur `Morriar` doit avoir accès à votre projet en mode *Developer*.

## Remise

La remise se fait automatiquement en ajoutant l'utilisateur `Morriar`
en mode *Developer*.

Le travail doit être remis au plus tard le:

* **5 octobre à 23h59** pour le groupe 21
* **7 octobre à 23h59** pour le groupe 40

À partir de minuit, une pénalité de **2%** par heure de retard sera appliquée.
